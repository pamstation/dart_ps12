import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:xml/xml.dart' as xml;
import 'package:http/http.dart' as http;

class PS12 {
  Uri baseUri;
  Button button;

  PS12(this.baseUri) {
    button = new Button(baseUri.replace(path: '/button'));
  }

  Protocol protocolFromFile(String file) =>
      new Protocol.fromFile(baseUri, file);

  Future<Protocol> getCurrentProtocol() async {
    var url = baseUri.replace(path: '/protocol');
    var resp = await http.get(url);
    if (resp.statusCode != 200) {
      var error = new PSError.fromXml(resp.statusCode, resp.body);
      if (error.code == 'protocol.null') return null;
      throw error;
    }
    return new Protocol(baseUri, resp.body);
  }

  Future ping() async {
    var url = baseUri.replace(path: '/ping');
    var resp = await http.get(url);
    if (resp.statusCode != 200) {
      throw 'ping fails : ${resp.body}';
    }
  }

  Future<FocusSettings> getFocusSetting() async {
    var url = baseUri.replace(path: '/focussettings');
    var resp = await http.get(url);
    if (resp.statusCode != 200) {
      throw 'getFocusSetting fails : ${resp.body}';
    }
    return new FocusSettings.fromXML(resp.body);
  }

  Future setFocusSetting(FocusSettings focusSettings) async {
    var url = baseUri.replace(path: '/focussettings');
    var resp = await http.post(url,
        body: focusSettings.toXML(),
        headers: {'content-type': 'application/x-www-form-urlencoded'});
    if (resp.statusCode != 200) {
      throw 'setFocusSetting fails : resp.statusCode ${resp.statusCode} : "${resp.body}" ${resp.headers}';
    }
  }

  Future<String> get status async {
    var url = baseUri.replace(path: '/status');
    var resp = await http.get(url);
    if (resp.statusCode != 200) {
      throw 'status fails : ${resp.body}';
    }
    var state;
    var document = xml.parse(resp.body);
    var serverElement = document.findAllElements('server').first;
    serverElement.attributes.forEach((a) {
      if (a.name.local == 'state') {
        state = a.value;
      }
    });
    return state;
  }

  Future turnOn() async {
    var url = baseUri.replace(path: '/turnOn');
    var resp = await http.get(url);
    if (resp.statusCode != 200) {
      throw 'turnOn fails : ${resp.body}';
    }
  }

  Future shutdown() async {
    var url = baseUri.replace(path: '/shutdown');
    var resp = await http.get(url);
    if (resp.statusCode != 200) {
      throw 'shutdown fails : ${resp.body}';
    }
  }
}

class Button {
  Uri uri;
  bool _status;
  StreamController _controller;

  Button(this.uri) {
    bool hasListener;
    _controller = new StreamController(onListen: () {
      hasListener = true;
      new Timer.periodic(new Duration(seconds: 1), (t) {
        if (!hasListener) {
          t.cancel();
          return;
        }
        new Future(() async {
          var s = await status;
          if (s != _status) {
            _status = s;
            _controller.add(_status);
          }
        });
      });
    }, onCancel: () {
      hasListener = false;
    });
  }

  Stream<bool> get onStateChange => _controller.stream;

  Future<bool> get status async {
    var resp = await http.get(uri);
    if (resp.statusCode != 200) {
      throw 'buttom status fails : ${resp.body}';
    }
    var status = false;
    var document = xml.parse(resp.body);

    var buttonElement = document.findAllElements('button').first;
    buttonElement.attributes.forEach((a) {
      if (a.name.local == 'status') {
//        print('button status  ${a.value }');
        status = a.value != '0';
      }
    });
    return status;
  }

  Future press() async {
    var resp = await http.put(uri);
    if (resp.statusCode != 200) {
      throw 'buttom press fails : ${resp.body}';
    }
  }
}

class PSError {
  String code;
  String description;

  PSError.fromXml(int httpStatus, String xmlStr) {
    code = 'unknown';
    description = '';
    xml.XmlDocument document;
    try {
      document = xml.parse(xmlStr);
      var buttonElement = document.findAllElements('error').first;
      buttonElement.attributes.forEach((a) {
        if (a.name.local == 'code') {
          code = a.value;
        }
        if (a.name.local == 'description') {
          description = a.value;
        }
      });
    } catch (e, st) {
      print(e);
      print(st);
      code = 'unknown';
      description = 'httpStatus ${httpStatus} : $xmlStr';
    }
  }
}

class Protocol {
  static const String initialized = 'initialized';
  static const String running = 'running';
  static const String aborting = 'aborting';
  static const String aborted = 'aborted';
  static const String completed = 'completed';

  Uri baseUri;

  String xmlProtocol;

  Protocol(this.baseUri, this.xmlProtocol);

  void setPath(String path) {
    xmlProtocol = xmlProtocol.replaceAll('{{path}}', path);
  }

  void setBarcodes(List<String> barcodes) {
    assert(barcodes.length == 3);
    xmlProtocol = xmlProtocol.replaceAll('{{barcode1}}', barcodes[0]);
    xmlProtocol = xmlProtocol.replaceAll('{{barcode2}}', barcodes[1]);
    xmlProtocol = xmlProtocol.replaceAll('{{barcode3}}', barcodes[2]);
  }

  Protocol.fromFile(this.baseUri, String file) {
    xmlProtocol = new File(file).readAsStringSync();
  }

  Future load() async {
    var url = baseUri.replace(path: '/protocol');
    var resp = await http.put(url,
        body: xmlProtocol,
        headers: {'content-type': 'application/x-www-form-urlencoded'});
    if (resp.statusCode != 200) {
      throw 'load fails : ${resp.body}';
    }
  }

  Future<List<ProtocolMessage>> msgQueue() async {
    var url = baseUri.replace(path: '/protocol/msgqueue');
    var resp = await http.get(url);
    if (resp.statusCode != 200) {
      throw 'msgQueue fails : ${resp.body}';
    }
    var document = xml.parse(resp.body);

    var list = document.findAllElements('list').first;

    return list.children.map((each) {
      return new ProtocolMessage.fromXmlNode(each);
    }).toList();
  }

  Future<String> getState() async {
    var msg = await msgQueue();
    List<StateMessage> list = msg.where((each) => each is StateMessage).toList();
    if (list.isEmpty) throw 'invalid state';
    return list.last.value;
  }

  Future<ErrorMessage> getError() async {
    var msg = await msgQueue();
    List<ErrorMessage> list = msg.where((each) => each is ErrorMessage).toList();
    if (list.isEmpty) return null;
    return list.first;
  }

  Future run() async {
    var url = baseUri.replace(path: '/protocol/run');
    var resp = await http.get(url);
    if (resp.statusCode != 200) {
      throw 'run fails : ${resp.body}';
    }
  }

  Future wait() async {
    while (!await isDone()) {
      var error = await getError();
      if (error != null) throw error;
      await new Future.delayed(new Duration(seconds: 5));
    }
  }

  Future<bool> isDone() async {
    var state = await getState();

    return state == completed || state == aborted;
  }

  Future unload() async {
    var url = baseUri.replace(path: '/protocol/unload');
    var resp = await http.get(url);
    if (resp.statusCode != 200) {
      throw 'unload fails : ${resp.body}';
    }
  }

  Future abort() async {
    var url = baseUri.replace(path: '/protocol/abort');
    var resp = await http.get(url);
    if (resp.statusCode != 200) {
      throw 'abort fails : ${resp.body}';
    }
  }
}

class FocusSettings {
  FocusSettings.fromXML(String str) {
    var document = xml.parse(str);

    print(str);

    var focusSettingElement = document.findAllElements('FocusSettings').first;

    focusSettingElement.attributes.forEach((a) {
      if (a.name.local == 'rPosForFilter1') {
        rPosForFilter1 = double.parse(a.value);
      } else if (a.name.local == 'rPosForFilter2') {
        rPosForFilter2 = double.parse(a.value);
      } else if (a.name.local == 'rPosForFilter3') {
        rPosForFilter3 = double.parse(a.value);
      } else if (a.name.local == 'rOffsetForDisp1') {
        rOffsetForDisp1 = double.parse(a.value);
      } else if (a.name.local == 'rOffsetForDisp2') {
        rOffsetForDisp2 = double.parse(a.value);
      } else if (a.name.local == 'rOffsetForDisp3') {
        rOffsetForDisp3 = double.parse(a.value);
      }
    });
  }
  double rPosForFilter1;
  double rPosForFilter2;
  double rPosForFilter3;

  double rOffsetForDisp1;
  double rOffsetForDisp2;
  double rOffsetForDisp3;

  String toXML() {
    return '<?xml version="1.0"?><settings><FocusSettings rPosForFilter1="$rPosForFilter1" '
        'rPosForFilter2="$rPosForFilter2" '
        'rPosForFilter3="$rPosForFilter3" '
        'rOffsetForDisp1="$rOffsetForDisp1" '
        'rOffsetForDisp2="$rOffsetForDisp2" '
        'rOffsetForDisp3="$rOffsetForDisp3"/></settings>';
  }
}

class ProtocolMessage {
  ProtocolMessage();
  factory ProtocolMessage.fromXmlNode(xml.XmlElement node) {
    ProtocolMessage msg;
    var name = node.name.local;
    switch (name) {
      case 'image':
        msg = new ImageMessage()..path = node.getAttribute('path');
        break;
      case 'protocolState':
        msg = new StateMessage()..value = node.getAttribute('value');
        break;
      case 'run':
        msg = new CurrentStepIndexMessage()
          ..index = int.parse(node.getAttribute('currentStepIndex'));
        break;
      case 'error':
        msg = new ErrorMessage()
          ..id = node.getAttribute('id')
          ..description = node.getAttribute('description');
        break;
      case 'incrementCycle':
        msg = new IncrementCycleMessage()
          ..cycle = int.parse(node.getAttribute('cycle'));
        break;
      case 'brokenMembrane':
        msg = new BrokenMembraneMessage()
          ..wellBitSet = int.parse(node.getAttribute('wellBitSet'));
        break;
      default:
        msg = new UnknownMessage()..name = node.name.local;
        break;
    }

    return msg;
  }
}

class ErrorMessage extends ProtocolMessage {
  String id;
  String description;

  String toString() => '${this.runtimeType}($id, $description)';
}

class ImageMessage extends ProtocolMessage {
  String path;
}

class StateMessage extends ProtocolMessage {
  String value;
}

class CurrentStepIndexMessage extends ProtocolMessage {
  int index;
}

class IncrementCycleMessage extends ProtocolMessage {
  int cycle;
}

class BrokenMembraneMessage extends ProtocolMessage {
  int wellBitSet;
}

class UnknownMessage extends ProtocolMessage {
  String name;
}
