# Setup

Install chocolatey. In an admin shell run the following 
 

```
@"%SystemRoot%\System32\WindowsPowerShell\v1.0\powershell.exe" -NoProfile -InputFormat None -ExecutionPolicy Bypass -Command "iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))" && SET "PATH=%PATH%;%ALLUSERSPROFILE%\chocolatey\bin"

```

Install dart-sdk using chocolatey, in a new admin shell run the following

```
choco install -y dart-sdk --version 1.23.0
```

Install git

```
choco install -y git
```

Then clone this repo. Open git command and execute the following

``` 
cd C:\PamSoft\PS12
git clone https://bitbucket.org/pamstation/dart_ps12.git
cd dart_ps12
pub get
```

# Usage

## Config

see bin/config.yaml


## Run the focus protocol

Start PSServer.exe.

``` 
cd C:\PamSoft\PS12\dart_ps12
run.bat
```

# Run loop

see bin/config-loop
 

``` 
cd C:\PamSoft\PS12\dart_ps12
run-loop.bat
```